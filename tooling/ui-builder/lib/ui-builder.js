#!/usr/bin/env node
const rollup = require('rollup')
const path = require('path')

const babel = require('@rollup/plugin-babel').default
const external = require('rollup-plugin-peer-deps-external')
const postcss = require('rollup-plugin-postcss-modules').default
const del = require('rollup-plugin-delete')
const typescript = require('rollup-plugin-typescript2')
const copy = require('rollup-plugin-copy')
// so JS can be rolled with TS
// remove when JS files have been removed
const nodeResolve = require('@rollup/plugin-node-resolve').default
const commonjs = require('@rollup/plugin-commonjs')

const extensions = ['.js', '.jsx', '.ts', '.tsx']

const currentWorkingPath = process.cwd()
const { source, name, ...pkg } = require(path.join(
  currentWorkingPath,
  'package.json'
))

const inputPath = path.join(currentWorkingPath, source)

const outputOptions = [
  {
    file: `dist/index.esm.js`,
    format: 'esm',
  },
  {
    file: `dist/index.cjs.js`,
    format: 'cjs',
  },
]

const options = {
  input: inputPath,
  output: outputOptions,
  plugins: [
    external(),
    typescript(),
    // so JS can be rolled with TS
    // remove when JS files have been removed
    nodeResolve({
      ignoreGlobal: false,
      include: ['node_modules/**'],
      extensions,
      // skip: keys(EXTERNALS), // <<-- skip: ['react', 'react-dom']
    }),
    commonjs({
      ignoreGlobal: false,
      include: 'node_modules/**',
    }),
    postcss({
      modules: true,
      extract: false,
      plugins: require('../postcss.config').plugins,
      minimize: true,
      autoModules: true,
      sourceMap: false,
    }),
    external(),
    babel({
      babelHelpers: 'runtime',
      exclude: 'node_modules/**',
      presets: [
        [
          '@babel/env',
          {
            modules: false,
            loose: true,
            targets: {
              browsers: ['>2%'],
            },
          },
        ],
        '@babel/preset-react',
      ],
      plugins: ['@babel/plugin-transform-runtime', 'add-react-displayname'],
    }),
    copy({
      targets: [{ src: 'src/*.css', dest: 'dist' }],
    }),
    del({ targets: ['dist/*'] }),
  ],
  external: [
    ...Object.keys(pkg.peerDependencies || {}),
    ...Object.keys(pkg.dependencies || {}),
  ],
}

async function build() {
  // create bundle
  const bundle = await rollup.rollup(options)
  // loop through the options and write individual bundles
  outputOptions.forEach(async (output) => {
    await bundle.write(output)
  })
}

build()
