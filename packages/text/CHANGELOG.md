# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.4.0 (2021-03-29)


### Features

* add text and button component ([83ba7e1](https://gitlab.com/mikqi/lerna-ci/commit/83ba7e14bf1df16651d245c0a2efce54611a3fa9))





# 1.3.0 (2021-03-26)


### Features

* add text and button component ([83ba7e1](https://gitlab.com/mikqi/lerna-ci/commit/83ba7e14bf1df16651d245c0a2efce54611a3fa9))





# 1.2.0 (2021-03-26)


### Features

* add text and button component ([83ba7e1](https://gitlab.com/mikqi/lerna-ci/commit/83ba7e14bf1df16651d245c0a2efce54611a3fa9))





# 1.1.0 (2021-03-24)


### Features

* add text and button component ([83ba7e1](https://gitlab.com/mikqi/lerna-ci/commit/83ba7e14bf1df16651d245c0a2efce54611a3fa9))
