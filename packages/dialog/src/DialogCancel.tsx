import * as React from 'react'
import { Primitive } from '@radix-ui/react-primitive'
import type * as Polymorphic from '@radix-ui/react-polymorphic'

const DESCRIPTION_NAME = 'DialogCancel'
const DESCRIPTION_DEFAULT_TAG = 'button'

type DialogCancelOwnProps = Polymorphic.OwnProps<typeof Primitive>
type DialogCancelPrimitive = Polymorphic.ForwardRefComponent<
  typeof DESCRIPTION_DEFAULT_TAG,
  DialogCancelOwnProps
>

export const DialogCancel = React.forwardRef((props, ref) => {
  const { as = DESCRIPTION_DEFAULT_TAG, ...cancelProps } = props
  return <Primitive type="button" ref={ref} as={as} {...cancelProps} />
}) as DialogCancelPrimitive

DialogCancel.displayName = DESCRIPTION_NAME
