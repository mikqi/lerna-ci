import * as React from 'react'
import { Primitive } from '@radix-ui/react-primitive'
import type * as Polymorphic from '@radix-ui/react-polymorphic'

const DESCRIPTION_NAME = 'DialogAction'
const DESCRIPTION_DEFAULT_TAG = 'button'

type DialogActionOwnProps = Polymorphic.OwnProps<typeof Primitive>
type DialogActionPrimitive = Polymorphic.ForwardRefComponent<
  typeof DESCRIPTION_DEFAULT_TAG,
  DialogActionOwnProps
>

export const DialogAction = React.forwardRef((props, ref) => {
  const { as = DESCRIPTION_DEFAULT_TAG, ...actionProps } = props
  return <Primitive type="button" ref={ref} as={as} {...actionProps} />
}) as DialogActionPrimitive

DialogAction.displayName = DESCRIPTION_NAME
