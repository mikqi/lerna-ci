import * as React from 'react'
import { Portal } from '@reach/portal'
import { Transition } from 'react-transition-group'
import { RemoveScroll } from 'react-remove-scroll'
import FocusLock from 'react-focus-lock'
import { DialogContextProvider } from './DialogContext'
import type { DialogContextType } from './DialogContext'

export type DialogProps = Omit<DialogContextType, 'transitionState'> & {
  children: React.ReactNode
}

export const Dialog = ({ children, ...props }: DialogProps) => {
  const {
    isOpen,
    onClose,
    size,
    animationDuration = { enter: 0, exit: 0 },
  } = props

  return (
    <DialogContextProvider value={{ isOpen, onClose, size, animationDuration }}>
      <Portal>
        <Transition
          appear
          in={isOpen}
          timeout={{
            enter: animationDuration.enter,
            exit: animationDuration.exit,
          }}
          unmountOnExit
        >
          {(state) => {
            return (
              <FocusLock autoFocus returnFocus>
                <RemoveScroll>
                  {React.Children.map(children, (child: any) =>
                    React.cloneElement(child, { transitionState: state })
                  )}
                </RemoveScroll>
              </FocusLock>
            )
          }}
        </Transition>
      </Portal>
    </DialogContextProvider>
  )
}

export * from './DialogContent'
export * from './DialogOverlay'
export * from './DialogDescription'
export * from './DialogAction'
export * from './DialogCancel'
export * from './DialogTitle'
export * from './DialogContext'
