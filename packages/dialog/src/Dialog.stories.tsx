import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import {
  Dialog,
  DialogContent,
  DialogAction,
  DialogCancel,
  DialogDescription,
  DialogTitle,
  DialogProps,
  DialogOverlay,
} from '.'

export default {
  title: 'Dialog',
  component: Dialog,
} as Meta

const ControlledComponent: Story<DialogProps> = (args) => {
  const [isOpen, setOpen] = React.useState(false)
  const [selectedName, setName] = React.useState<string | null>(null)

  function openDeleteModal(name: string) {
    setName(name)
    setOpen(true)
  }

  return (
    <>
      <ul>
        <button onClick={() => openDeleteModal('John')}>Hapus John</button>
        <button onClick={() => openDeleteModal('Jane')}>Hapus Jane</button>
        <button onClick={() => openDeleteModal('Jana')}>Hapus Jana</button>
      </ul>

      <Dialog isOpen={isOpen} size={args.size} onClose={() => setOpen(false)}>
        <DialogOverlay />
        <DialogContent>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <DialogTitle style={{ margin: '6px 0', textAlign: 'center' }}>
              Hapus user {selectedName} dari List?
            </DialogTitle>
            <DialogDescription style={{ textAlign: 'center', margin: '6px 0' }}>
              Kamu akan hapus user <em>{selectedName}</em> dari list, aksi ini
              tidak bisa dikembalikan.
            </DialogDescription>

            <DialogCancel
              style={{
                margin: '6px 0',
                width: '100%',
                background: 'transparent',
                border: 'none',
                fontWeight: 'bold',
              }}
              onClick={() => setOpen(false)}
            >
              Nah!
            </DialogCancel>
            <DialogAction style={{ margin: '6px 0', width: '100%' }}>
              Sure, delete him!
            </DialogAction>
          </div>
        </DialogContent>
      </Dialog>
    </>
  )
}
export const Controlled = ControlledComponent.bind({})
Controlled.argTypes = {
  isOpen: { control: false },
}
