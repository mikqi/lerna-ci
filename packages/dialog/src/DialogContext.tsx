import { createContext } from './utils'

export const [
  DialogContextProvider,
  useDialogContext,
] = createContext<DialogContextType>()

export interface DialogContextType {
  isOpen: boolean
  onClose: Function
  size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl'
  animationDuration?: {
    exit: number
    enter: number
  }
}
