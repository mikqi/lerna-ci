import * as React from 'react'
import { Primitive } from '@radix-ui/react-primitive'
import type * as Polymorphic from '@radix-ui/react-polymorphic'

const DESCRIPTION_NAME = 'DialogDescription'
const DESCRIPTION_DEFAULT_TAG = 'p'

type DialogDescriptionOwnProps = Polymorphic.OwnProps<typeof Primitive>
type DialogDescriptionPrimitive = Polymorphic.ForwardRefComponent<
  typeof DESCRIPTION_DEFAULT_TAG,
  DialogDescriptionOwnProps
>

export const DialogDescription = React.forwardRef((props, ref) => {
  const { as = DESCRIPTION_DEFAULT_TAG, ...descriptionProps } = props
  return (
    <Primitive
      about="primitive"
      as={as}
      ref={ref}
      id="dialog-description"
      {...descriptionProps}
    />
  )
}) as DialogDescriptionPrimitive

DialogDescription.displayName = DESCRIPTION_NAME
