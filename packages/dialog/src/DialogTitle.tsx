import * as React from 'react'
import { Primitive } from '@radix-ui/react-primitive'
import type * as Polymorphic from '@radix-ui/react-polymorphic'

const DESCRIPTION_NAME = 'DialogTitle'
const DESCRIPTION_DEFAULT_TAG = 'p'

type DialogTitleOwnProps = Polymorphic.OwnProps<typeof Primitive>
type DialogTitlePrimitive = Polymorphic.ForwardRefComponent<
  typeof DESCRIPTION_DEFAULT_TAG,
  DialogTitleOwnProps
>

export const DialogTitle = React.forwardRef((props, ref) => {
  const { as = DESCRIPTION_DEFAULT_TAG, ...titleProps } = props
  return <Primitive as={as} id="dialog-title" {...titleProps} ref={ref} />
}) as DialogTitlePrimitive

DialogTitle.displayName = DESCRIPTION_NAME
