import * as React from 'react'
import clsx from 'clsx'
import styles from './Dialog.module.css'
import { useDialogContext } from './DialogContext'

import type { TransitionStatus } from 'react-transition-group/Transition'

const sizesValue = {
  xs: '20rem',
  sm: '24rem',
  md: '28rem',
  lg: '32rem',
  xl: '36rem',
}

interface DialogContentProps extends React.HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode
  transitionState?: TransitionStatus
}
export const DialogContent = ({
  children,
  transitionState,
  ...props
}: DialogContentProps) => {
  const { size = 'sm', onClose } = useDialogContext()
  const classes = clsx(styles.dialogContent)

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Escape') {
      onClose()
    }
  }

  return (
    <div
      className={classes}
      data-state={transitionState}
      role="alertdialog"
      aria-modal="true"
      aria-labelledby="dialog-title"
      aria-describedby="dialog-description"
      style={{
        width: sizesValue[size],
      }}
      {...props}
      onKeyDown={handleKeyDown}
    >
      <div id="dialog-content">{children}</div>
    </div>
  )
}
