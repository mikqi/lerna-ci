import * as React from 'react'
import { Primitive } from '@radix-ui/react-primitive'
import type * as Polymorphic from '@radix-ui/react-polymorphic'

import styles from './Dialog.module.css'
import { useDialogContext } from './DialogContext'
import type { TransitionStatus } from 'react-transition-group'

const DESCRIPTION_NAME = 'DialogOverlay'
const DESCRIPTION_DEFAULT_TAG = 'p'

type DialogOverlayOwnProps = Polymorphic.OwnProps<typeof Primitive>
type DialogOverlayPrimitive = Polymorphic.ForwardRefComponent<
  typeof DESCRIPTION_DEFAULT_TAG,
  DialogOverlayOwnProps & {
    transitionState?: TransitionStatus
  }
>

export const DialogOverlay = React.forwardRef((props, ref) => {
  const { onClose } = useDialogContext()
  const {
    as = DESCRIPTION_DEFAULT_TAG,
    children,
    transitionState,
    ...overlayProps
  } = props
  return (
    <Primitive
      as={as}
      data-state={transitionState}
      onClick={() => {
        onClose && onClose()
      }}
      className={styles.dialogOverlay}
      {...overlayProps}
      ref={ref}
    >
      {children}
    </Primitive>
  )
}) as DialogOverlayPrimitive

DialogOverlay.displayName = DESCRIPTION_NAME
