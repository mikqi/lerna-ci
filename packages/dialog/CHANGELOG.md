# @mikqi/dialog

## 0.0.4

### Patch Changes

- export context for other consumer
- make animationDuration more specific

## 0.0.3

### Patch Changes

- fix: Missing types for autocomplete in dependencies dialog

## 0.0.2

### Patch Changes

- fix: DialogTrigger file included in build that makes build Dialog fails.

## 0.0.1

### Patch Changes

#### Dialog Component

- Rewrite component, create from scratch
- Use Radix UI Utilites
as