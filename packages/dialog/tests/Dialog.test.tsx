import { fireEvent, render, screen, testA11y } from '@mikqi/test-utils'
import * as React from 'react'
import {
  Dialog,
  DialogContent,
  DialogAction,
  DialogCancel,
  DialogDescription,
  DialogTitle,
  DialogOverlay,
} from '../src'

describe('Dialog Component', () => {
  test('dialog should work properly', () => {
    const handleClose = jest.fn()
    const title = 'Delete user John from List?'
    render(
      <>
        <button>Delete John</button>
        <Dialog
          onClose={handleClose}
          size="md"
          animationDuration={{ enter: 300, exit: 300 }}
          isOpen={true}
        >
          <DialogOverlay data-testid="overlay" />
          <DialogContent data-testid="content">
            <DialogTitle>{title}</DialogTitle>
            <DialogDescription>
              You will delete user John from list, this action cannot be undo.
            </DialogDescription>

            <DialogCancel onClick={handleClose}>Nah!</DialogCancel>
            <DialogAction>delete him!</DialogAction>
          </DialogContent>
        </Dialog>
      </>
    )

    expect(screen.getByText(title)).toBeInTheDocument()
    const nahButton = screen.getByText('Nah!')
    fireEvent.click(nahButton)
    expect(handleClose).toHaveBeenCalled()
    expect(handleClose).toHaveBeenCalledTimes(1)

    const overlay = screen.getByTestId('overlay')
    fireEvent.click(overlay)
    expect(handleClose).toHaveBeenCalledTimes(2)

    const content = screen.getByTestId('content')
    fireEvent.keyDown(content, { key: 'Escape', code: 'Escape' })
    expect(handleClose).toHaveBeenCalledTimes(3)

    fireEvent.keyDown(content, { key: 'Enter', code: 'Enter' })
    expect(handleClose).toHaveBeenCalledTimes(3)
  })

  test('should pass a11y', async () => {
    const { container } = render(
      <Dialog onClose={() => {}} isOpen={true}>
        <DialogOverlay as="section" />
        <DialogContent>
          <DialogTitle as="span">Delete user John from List?</DialogTitle>
          <DialogDescription as="div">
            You will delete user John from list, this action cannot be undo.
          </DialogDescription>

          <DialogCancel as="a">Nah!</DialogCancel>
          <DialogAction as="a">delete him!</DialogAction>
        </DialogContent>
      </Dialog>
    )
    await testA11y(container)
  })
})
