import * as React from 'react'
import clsx from 'clsx'
import { useDialogContext } from '@mikqi/dialog'
import { useSheetContext } from './SheetContext'
import styles from './Sheet.module.css'

const sizes = ['sm', 'md', 'lg', 'xl', 'full'] as const
const position = ['left', 'right', 'bottom', 'top'] as const

type SizeTypes = typeof sizes[number]
type PositionTypes = typeof position[number]

const sizesValue = {
  sm: 20,
  md: 40,
  lg: 60,
  xl: 80,
  full: 100,
}

const generateSize = (size: SizeTypes, position: PositionTypes) => {
  if (position === 'top' || position === 'bottom') {
    return {
      height: sizesValue[size] + 'vh',
    }
  }

  return {
    width: sizesValue[size] + 'vw',
  }
}

export const SheetContent = ({
  children,
  transitionState,
  className,
  ...props
}: any) => {
  const { onClose } = useDialogContext()
  const { position = 'bottom', sheetSize = 'md' } = useSheetContext()
  const contentClasses = clsx(styles.sheetContent, styles[position], className)

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Escape') {
      onClose()
    }
  }

  return (
    <div
      className={contentClasses}
      data-state={transitionState}
      role="dialog"
      aria-modal="true"
      aria-labelledby="sheet-title"
      aria-describedby="sheet-description"
      style={generateSize(sheetSize, position)}
      onKeyDown={handleKeyDown}
      {...props}
    >
      <div id="sheet-content">{children}</div>
    </div>
  )
}
