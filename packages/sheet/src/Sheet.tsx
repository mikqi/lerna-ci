import * as React from 'react'
import { Dialog, DialogProps } from '@mikqi/dialog'

import { SheetContextProvider } from './SheetContext'
import type { SheetContextType } from './SheetContext'

export type SheetProps = Omit<DialogProps, 'transitionState' | 'size'> &
  SheetContextType & {
    children: React.ReactNode
  }

export const Sheet = ({ children, ...props }: SheetProps) => {
  const {
    isOpen,
    onClose,
    animationDuration = { enter: 0, exit: 300 },
    sheetSize = 'md',
    position = 'bottom',
  } = props

  return (
    <SheetContextProvider value={{ sheetSize, position }}>
      <Dialog
        isOpen={isOpen}
        onClose={onClose}
        animationDuration={animationDuration}
      >
        {children}
      </Dialog>
    </SheetContextProvider>
  )
}

export { DialogOverlay as SheetOverlay } from '@mikqi/dialog'
export * from './SheetCloseButton'
export * from './SheetContent'
export * from './SheetTitle'
