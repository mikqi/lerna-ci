import * as React from 'react'
import { useDialogContext } from '@mikqi/dialog'
import { Primitive } from '@radix-ui/react-primitive'
import type * as Polymorphic from '@radix-ui/react-polymorphic'
import styles from './Sheet.module.css'

const DESCRIPTION_NAME = 'SheetCloseButton'
const DESCRIPTION_DEFAULT_TAG = 'button'

type SheetCloseButtonOwnProps = Polymorphic.OwnProps<typeof Primitive>
type SheetCloseButtonPrimitive = Polymorphic.ForwardRefComponent<
  typeof DESCRIPTION_DEFAULT_TAG,
  SheetCloseButtonOwnProps
>

export const SheetCloseButton = React.forwardRef((props, ref) => {
  const { as = DESCRIPTION_DEFAULT_TAG, className, ...buttonProps } = props
  const { onClose } = useDialogContext()
  return (
    <Primitive
      aria-label="Close"
      ref={ref}
      as={as}
      className={styles.sheetCloseBtn}
      onClick={() => onClose()}
      {...buttonProps}
    >
      <svg
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M4.16663 4.16667L9.99996 10M15.8333 15.8333L9.99996 10M9.99996 10L15.8333 4.16667L4.16663 15.8333"
          stroke="#CACCCF"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </Primitive>
  )
}) as SheetCloseButtonPrimitive

SheetCloseButton.displayName = DESCRIPTION_NAME
