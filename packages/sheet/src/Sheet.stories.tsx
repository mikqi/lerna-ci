import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import {
  Sheet,
  SheetCloseButton,
  SheetContent,
  SheetTitle,
  SheetProps,
  SheetOverlay,
} from '.'

export default {
  title: 'Sheet',
  component: Sheet,
} as Meta

const ControlledTemplate: Story<SheetProps> = (args) => {
  const [isOpen, setOpen] = React.useState(false)

  return (
    <>
      <button onClick={() => setOpen(true)}>Buka Sheet</button>
      <Sheet
        position={args.position}
        sheetSize={args.sheetSize}
        isOpen={isOpen}
        onClose={setOpen}
      >
        <SheetOverlay as="span" />
        <SheetContent>
          <SheetTitle>Overlay Sheet</SheetTitle>
          <div>Lorem</div>
          <SheetCloseButton onClick={() => setOpen(false)} />
        </SheetContent>
      </Sheet>
    </>
  )
}

export const Controlled = ControlledTemplate.bind({})
Controlled.argTypes = {
  isOpen: { control: false },
  onClose: { control: false },
  animationDuration: { control: false },
}
