import { createContext } from './utils'

export const [
  SheetContextProvider,
  useSheetContext,
] = createContext<SheetContextType>()

export interface SheetContextType {
  sheetSize?: 'sm' | 'md' | 'lg' | 'xl' | 'full'
  position?: 'left' | 'right' | 'bottom' | 'top'
}
