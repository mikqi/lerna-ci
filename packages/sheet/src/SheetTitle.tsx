import * as React from 'react'
import clsx from 'clsx'
import { Primitive } from '@radix-ui/react-primitive'
import type * as Polymorphic from '@radix-ui/react-polymorphic'
import styles from './Sheet.module.css'

const DESCRIPTION_NAME = 'SheetTitle'
const DESCRIPTION_DEFAULT_TAG = 'h2'

type SheetTitleOwnProps = Polymorphic.OwnProps<typeof Primitive>
type SheetTitlePrimitive = Polymorphic.ForwardRefComponent<
  typeof DESCRIPTION_DEFAULT_TAG,
  SheetTitleOwnProps
>

export const SheetTitle = React.forwardRef((props, ref) => {
  const { as = DESCRIPTION_DEFAULT_TAG, className, ...cancelProps } = props
  return (
    <Primitive
      ref={ref}
      as={as}
      className={clsx(styles['sheet-header'], className)}
      {...cancelProps}
    />
  )
}) as SheetTitlePrimitive

SheetTitle.displayName = DESCRIPTION_NAME
