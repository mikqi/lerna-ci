const baseConfig = require('../../jest.config')

module.exports = {
  ...baseConfig,
  transform: {
    '.(ts|tsx)$': 'ts-jest/dist',
    '.(css|scss)$': '../../config/jest.style-mock.js',
  },
  collectCoverageFrom: [...baseConfig.collectCoverageFrom, '!src/utils.ts'],
}
