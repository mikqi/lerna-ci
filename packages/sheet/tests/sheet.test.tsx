import { fireEvent, render, screen, testA11y } from '@mikqi/test-utils'
import * as React from 'react'
import {
  Sheet,
  SheetCloseButton,
  SheetContent,
  SheetTitle,
  SheetOverlay,
} from '../src'

describe('Sheet Component', () => {
  test('sheet should work properly', () => {
    const handleClose = jest.fn()
    const title = 'Overlay sheet'
    render(
      <>
        <button>Open Sheet</button>
        <Sheet
          onClose={handleClose}
          sheetSize="md"
          animationDuration={{ enter: 300, exit: 300 }}
          isOpen={true}
        >
          <SheetOverlay as="span" data-testid="sheet-overlay" />
          <SheetContent data-testid="content">
            <SheetTitle>{title}</SheetTitle>
            <div>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum
              repellat beatae soluta blanditiis nemo dicta, pariatur itaque,
              modi eligendi corporis assumenda, odio quod voluptas ratione a?
              Quod illo molestiae sed.
            </div>
            <SheetCloseButton data-testid="close-btn" as="span" />
          </SheetContent>
        </Sheet>
      </>
    )

    expect(screen.getByText(title)).toBeInTheDocument()

    const closeButton = screen.getByTestId('close-btn')
    fireEvent.click(closeButton)
    expect(handleClose).toHaveBeenCalledTimes(1)

    const overlay = screen.getByTestId('sheet-overlay')
    fireEvent.click(overlay)
    expect(handleClose).toHaveBeenCalledTimes(2)

    const content = screen.getByTestId('content')
    fireEvent.keyDown(content, { key: 'Escape', code: 'Escape' })
    expect(handleClose).toHaveBeenCalledTimes(3)

    fireEvent.keyDown(content, { key: 'Enter', code: 'Enter' })
    expect(handleClose).toHaveBeenCalledTimes(3)
  })

  test('should pass a11y', async () => {
    const handleClose = jest.fn()
    const title = 'Overlay sheet'
    const { container } = render(
      <Sheet position="right" onClose={handleClose} isOpen={true}>
        <SheetOverlay as="span" />
        <SheetContent>
          <SheetTitle as="span">{title}</SheetTitle>
          <div>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum
            repellat beatae soluta blanditiis nemo dicta, pariatur itaque, modi
            eligendi corporis assumenda, odio quod voluptas ratione a? Quod illo
            molestiae sed.
          </div>
          <SheetCloseButton />
        </SheetContent>
      </Sheet>
    )

    await testA11y(container)
  })
})
