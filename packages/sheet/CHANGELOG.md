# @mikqi/sheet

## 0.1.2

### Patch Changes

**FIX**

- Remove margin from title

## 0.1.1

### Patch Changes

### Feature

- New Sheet component fully support a11y
- add fallback to avatar component when image is unavailable
- add first letter to fallback from `name` props when image is unavailable
awas